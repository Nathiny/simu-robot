from PyQt5 import QtCore, QtGui, QtWidgets
import time
import math


def init_interface(ui, MainWindow):
    """ Initialise l'interface """
    ui.setupUi(MainWindow)

    ui.simuScene = QtWidgets.QGraphicsScene()
        
    ui.simuView.setScene(ui.simuScene)
    img = QtGui.QPixmap("table.png")
    ui.simuScene.addPixmap(img)
    ui.simuView.zoom_view(0.26)
    


class PanZoomView(QtWidgets.QGraphicsView):
    """ Une vue intéractive qui comprend la fonction Zoom """

    def __init__(self, scene):
        super().__init__(scene)
        # Active l'anti-aliasing
        self.setRenderHint(QtGui.QPainter.Antialiasing)
        # Active le drag and drop de la vue
        self.setDragMode(self.ScrollHandDrag)

    def wheelEvent(self, event):
        """ Zoome lorsque la molette est utilisée """
        factor = math.pow(1.001, event.angleDelta().y())
        self.zoom_view(factor)

    @QtCore.pyqtSlot(int)
    def zoom_view(self, factor):
        """ Met à jour le facteur de zoom """
        self.setTransformationAnchor(self.AnchorUnderMouse)
        super().scale(factor, factor)
