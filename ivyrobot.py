#!/usr/bin/python

import time
from PyQt5 import QtCore, QtGui, QtWidgets
import interface
import gui
import sys
import numpy as np
import math


from threading import Thread


from ivy.std_api import IvyInit, IvyStart, IvyBindMsg, IvySendMsg

BLACK = QtCore.Qt.black
GREEN = QtCore.Qt.green
YELLOW = QtCore.Qt.yellow
RED = QtCore.Qt.red
SPEED = 1
X_MAX = 3000
Y_MAX = 2000
EPS = 30

with open("DEFAULT_BUS") as f:
    a = f.readline()

DEFAULT_BUS = a
ivyname = "simu_rob"

IvyInit(ivyname)
IvyStart(DEFAULT_BUS)


def norm(x, y):
    return (x**2 + y**2)**(1/2)


def draw(rect, x, y, theta, h, w):

    h2 = int(h/2)
    w2 = int(w/2)
    rect.setTransformOriginPoint(w / 2, h / 2)
    rect.setRotation(theta)

    rect.setPos(x - h2, y - w2)


class SimpleActuator():

    def __init__(self, robot, name):
        self.robot = robot
        self.name = name
        IvyBindMsg(self.get_actuators, robot + " get_actuators$")

    def get_actuators(self, sender):
        IvySendMsg(self.robot + " " + self.name)


class Servo(SimpleActuator):

    def __init__(self, robot, name, angle=0):
        super().__init__(robot, name)
        self.angle = angle
        IvyBindMsg(self.set_angle, self.robot + " " + self.name + " set_angle" + " ([-|+]*[0-9]*).*$")
        IvyBindMsg(self.get_angle, self.robot + " " + self.name + " get_angle$")

    def set_angle(self, sender, angle):
        self.angle = angle

    def get_angle(self, sender):
        IvySendMsg(self.robot + " " + self.name + " angle : " + str(self.angle))


class Motor(SimpleActuator):

    def __init__(self, robot, name, speed=0, clockwise=True):
        super().__init__(robot, name)
        self.speed = speed
        self.clockwise = clockwise 
        IvyBindMsg(self.set_speed, self.robot + " " + self.name + " set_speed" + " ([+]*[0-9]*).*$")
        IvyBindMsg(self.set_clockwise, self.robot + " " + self.name + " set_clockwise" + " ([0-1]{1}).*$")
        IvyBindMsg(self.get_speed, self.robot + " " + self.name + " get_speed$")

    def set_speed(self, sender, speed):
        self.speed = speed

    def set_clockwise(self, sender, rotation):
        self.clockwise = bool(int(rotation))

    def get_speed(self, sender):
        IvySendMsg(self.robot + " " + self.name + " speed : " + str(self.speed) + " clockwise : " + str(self.clockwise))


class Valve(SimpleActuator):

    def __init__(self, robot, name, open=True):
        super().__init__(robot, name)
        self.open = open
        IvyBindMsg(self.set_state, self.robot + " " + self.name + " set_state " + "([0-1]{1}).*$")
        IvyBindMsg(self.get_state, self.robot + " " + self.name + " get_state$")

    def set_state(self, sender, open):
        self.open = bool(int(open))

    def get_state(self, sender):
        IvySendMsg(self.robot + " " + self.name + " opened : " + str(self.open))


class Pump(SimpleActuator):

    def __init__(self, robot, name, power=0, smtCatched=False):
        super().__init__(robot, name)
        self.power = power
        self.smtCatched = smtCatched 
        IvyBindMsg(self.set_power, self.robot + " " + self.name + " set_power" + " ([+]*[0-9]*).*$")
        IvyBindMsg(self.set_smtCatched, self.robot + " " + self.name + " set_smtCatched" + " ([0-1]{1}).*$")
        IvyBindMsg(self.get_state, self.robot + " " + self.name + " get_state$")

    def set_power(self, sender, power):
        self.power = power

    def set_smtCatched(self, sender, smtCatched):
        self.smtCatched = bool(int(smtCatched))

    def get_state(self, sender):
        IvySendMsg(self.robot + " " + self.name + " power : " + str(self.power) + " smtCatched : " + str(self.smtCatched))



class Robot(QtCore.QThread):

    updated = QtCore.pyqtSignal(
        QtWidgets.QGraphicsRectItem, int, int, int, int, int)

    def __init__(self, name, x, y, theta, nominalSpeed, h, w, color):

        super().__init__()
        self.name = name
        self.speed_x = 0
        self.speed_y = 0
        self.theta = theta  # deg
        self.omega = 0  # deg/s
        self.x = x
        self.y = y
        self.nominalSpeed = nominalSpeed


        self.trajectory_id = ""

        self.target_x = 0
        self.target_y = 0
        self.target_theta = 0
        self.target_go = False

        self.h = h
        self.w = w
        self.color = color

        self.rect = QtWidgets.QGraphicsRectItem()
        self.rect.setRect(QtCore.QRectF(0, 0, self.h, self.w))
        self.rect.setPos(self.x - self.h / 2, self.y - self.w / 2)
        self.rect.setTransformOriginPoint(self.w / 2, self.h / 2)
        self.rect.setRotation(self.theta)

        self.rect.setBrush(QtGui.QBrush(self.color))

        # dictionnary of @param simpleActuator {String name: SimpleActuator(String name, String state)}
        self.simpleActuators = {}

        IvyBindMsg(self.send_trajectory, name + " get_trajectory")
        IvyBindMsg(self.send_pos, name + " get_pos")
        IvyBindMsg(self.send_theta, name + " get_theta")
        IvyBindMsg(self.send_speed, name + " get_speed")
        IvyBindMsg(self.send_omega, name + " get_omega")
        IvyBindMsg(self.set_speed_x, self.name + " set_speed ([-|+]*[0-9]*)$")
        IvyBindMsg(self.set_speed_x, self.name + " set_speed_x ([-|+]*[0-9]*)$")
        IvyBindMsg(self.set_speed_y, self.name + " set_speed_y ([-|+]*[0-9]*)$")
        IvyBindMsg(self.tp, self.name +
                   " tp ([-|+]*[0-9]*) ([-|+]*[0-9]*) ([-|+]*[0-9]*)$")
        IvyBindMsg(self.tp, self.name + " tp ([-|+]*[0-9]*) ([-|+]*[0-9]*)$")
        IvyBindMsg(self.start_trajectory, name +
                   " go_to ([-|+]*[0-9]*) ([-|+]*[0-9]*) ([-|+]*[0-9]*)$")

        IvyBindMsg(self.start_trajectory, name +
                   " go_to ([-|+]*[0-9]*) ([-|+]*[0-9]*) ([-|+]*[0-9]*) ([a-zA-Z0-9]+)$")

        IvyBindMsg(self.start_trajectory, name +
                   " go_to ([-|+]*[0-9]*) ([-|+]*[0-9]*)$")
        IvyBindMsg(self.go_theta, name + " go_theta ([-|+]*[0-9]*)$")
        IvyBindMsg(self.set_omega, self.name + " set_omega ([-|+]*[0-9]*)")
        IvyBindMsg(self.tp, self.name +
                   " tp ([-|+]*[0-9]*) ([-|+]*[0-9]*) ([-|+]*[0-9]*)")

    def send_trajectory(self, _):
        if self.target_go:
            IvySendMsg(self.name + " start " + str(self.x) + " " + str(self.y) + " end " + str(int(self.target_x)) + " " + str(int(self.target_y)))

    def send_pos(self, _):
        IvySendMsg(self.name + " x " + str(self.x) + " y " + str(self.y) + " theta " + str(self.theta%360))

    def send_theta(self, _):
        IvySendMsg(self.name + " theta " + str(self.theta))
    def send_speed(self, _):
        IvySendMsg(self.name + " v " + str(norm(self.speed_x, self.speed_y)))

    def send_omega(self, _):
        IvySendMsg(self.name + " omega " + str(self.omega))

    def set_speed(self, sender, v):
        self.speed_x = v * 1000 / 24
        self.speed_y = 0

    def set_speed_x(self, sender, v):
        self.speed_x = float(v) * 1000 / 24
    
    def set_speed_y(self, sender, v):
        self.speed_y = float(v) * 1000 / 24

    def tp(self, sender, x, y, theta=0):
        self.x = int(x)
        self.y = int(y)
        self.theta = int(theta)

    def set_omega(self, sender, omega):
        self.omega = float(omega)

    def addSimpleActuator(self, name, state):
        if not name in self.simpleActuators:
            self.simpleActuators[name] = SimpleActuator(name, state)
        else:
            print("Nom d'actionneur " + name + " déjà utilisé !")
    
    def add_actuator(self, actu):
        '''
        robot.add_actuators("servo1", Servo("robot_a", "servo1", 10))
        robot.add_actuators(motor.name, motor)...
        '''
        name = actu.name
        if not name in self.simpleActuators:
            self.simpleActuators[name]=actu
        else:
            print("Nom d'actionneur " + name + " déjà utilisé !")

    @QtCore.pyqtSlot()
    def update(self):
        self.theta = int(self.theta + self.omega) % 360
        self.x += math.floor(np.cos(np.deg2rad(self.theta)) * self.speed_x - np.sin(np.deg2rad(self.theta)) * self.speed_y)
        self.y += math.floor(np.sin(np.deg2rad(self.theta)) * self.speed_x + np.cos(np.deg2rad(self.theta)) * self.speed_y)
        if self.target_go:
            self.go_to()
        else:
            if self.x > X_MAX-self.h/2-EPS or self.y > Y_MAX-self.h/2-EPS or self.x < EPS+self.h/2 or self.y < EPS+self.h/2:
                self.set_speed("null", 0)
            elif self.x > X_MAX-self.h/2-EPS*3:
                self.theta = 0
                self.set_speed("null", self.nominalSpeed/5)
            elif self.y > Y_MAX-self.h/2-EPS*3:
                self.theta = 90
                self.set_speed("null", self.nominalSpeed/5)
            elif self.x < EPS*3+self.h/2:
                self.theta = 180
                self.set_speed("null", self.nominalSpeed/5)
            elif self.y < EPS*3+self.h/2:
                self.theta = 270
                self.set_speed("null", self.nominalSpeed/5)


        
        self.updated.emit(self.rect, self.x, self.y,
                          self.theta, self.h, self.w)

    def go_theta(self, sender, theta):
        self.theta = int(theta)

    def start_trajectory(self, sender, x, y, theta=-1000, traj_name=""):
        self.trajectory_id = traj_name
        diag = norm(self.h, self.w) / 2
        self.target_x = max(diag, min(int(x), X_MAX-diag))
        self.target_y = max(diag, min(int(y), Y_MAX-diag))
        self.target_theta = int(theta)
        self.target_forward = True
        self.target_go = True

    def go_to(self):
        self.send_trajectory("")
        self.theta = np.rad2deg(np.arctan2(
            self.target_y - self.y, self.target_x - self.x))
        self.set_speed("null", self.nominalSpeed)

        if (self.target_theta == -1000):
            self.target_theta = self.theta

        if norm(self.target_x - self.x, self.target_y - self.y) < 5:
            self.set_speed("null", 0)
            self.theta = self.target_theta
            self.target_go = False
            print(self.trajectory_id)
            if (self.trajectory_id != ""):
                IvySendMsg(self.trajectory_id + " trajectory_finished True")

        elif norm(self.target_x - self.x, self.target_y - self.y) < 50:
            self.set_speed("null", self.nominalSpeed / 5)

        self.theta = int(self.theta)
        self.speed_x = int(self.speed_x)
        self.speed_y = int(self.speed_y)

    def run(self):
        while(True):
            self.update()
            time.sleep(1/24)


app = QtWidgets.QApplication(sys.argv)
MainWindow = QtWidgets.QMainWindow()
ui = gui.Ui_MainWindow()
interface.init_interface(ui, MainWindow)


robot_a = Robot("Alice", 1500, 1000, 20, 1, 200, 150, YELLOW)
robot_b = Robot("Edgar", 80, 1000, 0, 1, 200, 150, RED)

robot_a.add_actuator(Servo(robot_a.name, "servo1", 10))

m1 = Motor(robot_a.name, "motor1")
p1 = Pump(robot_a.name, "pump1", 12, True)
v1 = Valve(robot_a.name, "valve1")

robot_a.add_actuator(m1)
robot_a.add_actuator(p1)
robot_a.add_actuator(v1)

s2 = Servo(robot_b.name, "servo1", 10)
s3 = Servo(robot_b.name, "servo2", 10)
robot_b.add_actuator(s2)
robot_b.add_actuator(s3)


ui.simuScene.addItem(robot_a.rect)
ui.simuScene.addItem(robot_b.rect)

robot_a.start()
# robot_a.addSimpleActuator("pump1", "on")
robot_b.start()

MainWindow.show()

robot_a.updated.connect(draw)
robot_b.updated.connect(draw)

sys.exit(app.exec_())
