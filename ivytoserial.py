import time
import serial
from ivy.std_api import IvyInit, IvyStart, IvyBindMsg, IvySendMsg
import numpy as np
import math

with open("DEFAULT_BUS") as f:
    a = f.readline()

fps = 60

DEFAULT_BUS = a
ivyname = "simu_rob"

IvyInit(ivyname)
IvyStart(DEFAULT_BUS)

print(DEFAULT_BUS)

SPEED_MMS = 600
OMEGA_DEGS = 350
X_MAX = 10000
Y_MAX = 10000

def norm(x, y):
    return (x**2 + y**2)**(1/2)

def clamp(n, smallest, largest): 
    return max(smallest, min(n, largest))

class Robot():

    def __init__(self, name, x, y, theta, h, w, rapport, wheelbase):
        super().__init__()
        self.ser = serial.Serial(
            port='/dev/ttyACM1',
            baudrate=115200
        )

        self.ser.isOpen()

        self.name = name
        self.pred = 100000000
    
        self.speed_mms = 0 # mm/s
        self.pwm_r = 0
        self.pwm_l = 0
        self.rapport = rapport
        self.wheelbase = wheelbase

        self.direction_r = 1
        self.direction_l = 1

        self.theta = theta  # deg
        self.omega = 0  # deg/s
        self.x = x
        self.y = y

        self.target_x = 0
        self.target_y = 0
        self.target_theta = 0
        self.target_go = False
        self.target_state = "null"

        self.h = h
        self.w = w

        # dictionnary of @param simpleActuator {String name: SimpleActuator(String name, String state)}
        self.simpleActuators = {}

        IvyBindMsg(self.stop, name + " stop")
        IvyBindMsg(self.send_pos, name + " get_pos")
        IvyBindMsg(self.send_theta, name + " get_theta")
        IvyBindMsg(self.send_speed, name + " get_speed")
        IvyBindMsg(self.send_omega, name + " get_omega")
        IvyBindMsg(self.set_speed, self.name + " set_speed ([-|+]*[0-9]*)$")
        IvyBindMsg(self.set_omega, self.name + " set_omega ([-|+]*[0-9]*)")
        IvyBindMsg(self.tp, self.name +
                   " tp ([-|+]*[0-9]*) ([-|+]*[0-9]*) ([-|+]*[0-9]*)$")
        IvyBindMsg(self.tp, self.name + " tp ([-|+]*[0-9]*) ([-|+]*[0-9]*)$")
        IvyBindMsg(self.start_trajectory, name +
                   " go_to ([-|+]*[0-9]*) ([-|+]*[0-9]*) ([-|+]*[0-9]*)$")
        IvyBindMsg(self.start_trajectory, name +
                   " go_to ([-|+]*[0-9]*) ([-|+]*[0-9]*) .*")
        IvyBindMsg(self.go_theta, name + " go_theta ([-|+]*[0-9]*)$")
        IvyBindMsg(self.tp, self.name +
                   " tp ([-|+]*[0-9]*) ([-|+]*[0-9]*) ([-|+]*[0-9]*)")

    def send_pos(self, _):
        IvySendMsg(self.name + " x " + str(self.x) + " y " + str(self.y) + " theta " + str(self.theta))

    def send_theta(self, _):
        IvySendMsg(self.name + " theta " + str(self.theta))

    def send_speed(self, _):
        IvySendMsg(self.name + " v " + str(norm(self.speed_mms)))

    def send_omega(self, _):
        IvySendMsg(self.name + " omega " + str(self.omega))

    def stop(self, _):
        inputq = "stop"
        self.ser.write(inputq.encode() + b'\r\n')
        self.ser.write(inputq.encode() + b'\r\n')

    def set_speed(self, sender, v):
        v = int(v)
        if v == 0:
            self.speed_mms = 0
            self.stop("null")
            return
        v = v / abs(v) * SPEED_MMS
        self.direction_r = v / abs(v)
        self.direction_l = v / abs(v)
        self.speed_mms = v
        self.speed_pwm_r = clamp(10000, 0, 10000)
        self.speed_pwm_l = clamp(10000, 0, 10000)
        inputq = ""
        if self.direction_l > 0:
            inputq = "forward " + str(int(self.speed_pwm_l))
        else:
            inputq = "reverse " + str(int(self.speed_pwm_l))
        self.ser.write(inputq.encode() + b'\r\n')

    def set_omega(self, sender, omega):
        omega = int(omega)
        if omega == 0:
            self.omega = 0
            self.stop("null")
            return
        omega = omega / abs(omega) * OMEGA_DEGS
        self.direction_r = omega / abs(omega)
        self.direction_l = -omega / abs(omega)

        self.omega = OMEGA_DEGS
        self.speed_pwm_l = int(clamp(abs(self.omega*self.wheelbase), 0, 10000))
        self.speed_pwm_r = int(clamp(abs(self.omega*self.wheelbase), 0, 10000))
        if self.direction_l < 0:
            inputq = "right " + str(int(self.speed_pwm_l))
        else:
            inputq = "left " + str(int(self.speed_pwm_l))
        self.ser.write(inputq.encode() + b'\r\n')

    def tp(self, sender, x, y, theta=0):
        self.x = int(x)
        self.y = int(y)
        self.theta = int(theta)

    def start_trajectory(self, sender, x, y, theta=-1000):
        diag = norm(self.h, self.w) / 2
        self.target_x = max(diag, min(int(x), X_MAX-diag))
        self.target_y = max(diag, min(int(y), Y_MAX-diag))
        self.cruise_theta = np.rad2deg(np.arctan2(
            self.target_y - self.y, self.target_x - self.x))
        self.target_theta = int(self.cruise_theta)
        self.target_forward = True
        self.target_go = True
        self.target_state = "initial turn"
        self.pred = 1000000000

    def go_to(self):

        print(norm(self.target_x - self.x, self.target_y - self.y), self.pred)

        if (self.target_state == "initial turn"):
            if abs(self.theta - self.cruise_theta) % 360 < 10:
                self.set_omega("null", 0)
                self.target_state = "cruise"
                self.theta = self.cruise_theta
            else:
                self.set_omega("null", OMEGA_DEGS)


        elif (self.target_state == "cruise"):
            actu_norm = norm(self.target_x - self.x, self.target_y - self.y)
            if actu_norm < 70 or actu_norm > self.pred :
                self.set_speed("null", 0)
                self.target_go = False
                self.x = self.target_x
                self.y = self.target_y
                self.target_state = "final turn"
            else:
                self.set_speed("null", SPEED_MMS)
                self.pred = actu_norm

        
        else:
            if abs(self.theta - self.target_theta) % 360 < 10:
                self.theta = self.target_theta
                self.set_omega("null", 0)
                self.set_speed("null", 0)
                self.stop("null")
                self.target_state = "null"
                
            else:
                self.set_omega("null", OMEGA_DEGS)

    def go_theta(self, sender, theta):
        start_trajectory(sender, self.x, self.y, theta)

    def update(self):
        self.theta = int(self.theta - self.direction_l*self.omega/fps) % 360
        self.x += math.floor(np.cos(np.deg2rad(self.theta)) * self.speed_mms/fps)
        self.y += math.floor(np.sin(np.deg2rad(self.theta)) * self.speed_mms/fps)

        if self.target_go:
            self.go_to()



    def run(self):
        while(True):
            self.update()
            time.sleep(1/fps)



# Robot(self, name, x, y, theta, h, w, rapport, wheelbase)

robot_a = Robot("Real", 1500, 1000, 0, 200, 150, 18, 56)
robot_a.run()
